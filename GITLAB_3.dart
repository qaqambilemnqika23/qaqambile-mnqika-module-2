class Developer {
  String nameOfApp = '(insert name)';
  String category = '(insert category/section)';
  String nameOfDev = '(insert developer name)';
  int year = 0;
  
 @override /*helps when class is not in null safety*/
  String toString (){
    return 'Name of App: $nameOfApp, Category/Sector: $category ,Name of Developer: $nameOfDev , Year won: $year ';
  }
  
 
  
  void information (){
    nameOfApp.toUpperCase();
    
    print(nameOfApp.toUpperCase());
  }
}


void main() {
  final dev1 = Developer();
  final dev2 = Developer();
  final dev3 = Developer();
  final dev4 = Developer();
  final dev5 = Developer();
  final dev6 = Developer();
  final dev7 = Developer();
  final dev8 = Developer();
  final dev9 = Developer();
  final dev10 = Developer();
  
  dev1.nameOfApp = 'FNB App';
  dev1.category = 'Finance/Banking';
  dev1.nameOfDev = 'FirstRand Limited';
  dev1.year = 2012;
  
  dev2.nameOfApp = 'SnapScan';
  dev2.category = 'Finance';
  dev2.nameOfDev = 'FireID Payments';
  dev2.year = 2013;
  
  dev3.nameOfApp = 'LIVE Inspect';
  dev3.category = 'Productivity';
  dev3.nameOfDev = 'Radweb';
  dev3.year = 2014;
  
  dev4.nameOfApp = 'WumDrop';
  dev4.category = 'Maps and Navigation';
  dev4.nameOfDev = 'WumDrop';
  dev4.year = 2015;
  
  dev5.nameOfApp = 'Domestly';
  dev5.category = 'Lifestyle';
  dev5.nameOfDev = 'Rachel Jones';
  dev5.year = 2016;
  
  dev6.nameOfApp = 'Shyft';
  dev6.category = 'Finance/Forex';
  dev6.nameOfDev = 'Standard Bank Shyft';
  dev6.year = 2017;
  
  dev7.nameOfApp = 'Khula Ecosystem';
  dev7.category = 'Farming';
  dev7.nameOfDev = 'Khula!';
  dev7.year = 2018;
  
  dev8.nameOfApp = 'Naked Insurance';
  dev8.category = 'Insurance';
  dev8.nameOfDev = 'Naked Insurance';
  dev8.year = 2019;
  
  dev9.nameOfApp = 'Easy Equities';
  dev9.category = 'Forex/Finance';
  dev9.nameOfDev = 'First World Traders';
  dev9.year = 2020;
  
  dev10.nameOfApp = 'Edtech';
  dev10.category = 'Education';
  dev10.nameOfDev = 'Ambani Africa';
  dev10.year = 2021;
  
 
  print(dev1);
  print(dev2);
  print(dev3);
  print(dev4);
  print(dev5);
  print(dev6);
  print(dev7);
  print(dev8);
  print(dev9);
  print(dev10);
  
  }
